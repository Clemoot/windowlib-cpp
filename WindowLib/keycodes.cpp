#include "keycodes.h"
#include <windows.h>

const char* WindowLib::KeyCodeMaps::KeyCodeInt_To_String[] = {
    "Unknown",
    "Backspace",
    "Tab",
    "Enter",
    "Shift",
    "Control",
    "Alt",
    "CapsLock",
    "Escape",
    "Spacebar",
    "PageUp",
    "PageDown",
    "LeftArrow",
    "RightArrow",
    "UpArrow",
    "DownArrow",
    "PrintScreen",
    "Insert",
    "Delete",
    "Attn",
    "Pause",
    "Key0",
    "Key1",
    "Key2",
    "Key3",
    "Key4",
    "Key5",
    "Key6",
    "Key7",
    "Key8",
    "Key9",
    "KeyA",
    "KeyB",
    "KeyC",
    "KeyD",
    "KeyE",
    "KeyF",
    "KeyG",
    "KeyH",
    "KeyI",
    "KeyJ",
    "KeyK",
    "KeyL",
    "KeyM",
    "KeyN",
    "KeyO",
    "KeyP",
    "KeyQ",
    "KeyR",
    "KeyS",
    "KeyT",
    "KeyU",
    "KeyV",
    "KeyW",
    "KeyX",
    "KeyY",
    "KeyZ",
    "Windows",
    "Numpad0",
    "Numpad1",
    "Numpad2",
    "Numpad3",
    "Numpad4",
    "Numpad5",
    "Numpad6",
    "Numpad7",
    "Numpad8",
    "Numpad9",
    "NumpadMultiply",
    "NumpadAdd",
    "NumpadSubstract",
    "NumpadDivide",
    "NumpadDecimal",
    "F1",
    "F2",
    "F3",
    "F4",
    "F5",
    "F6",
    "F7",
    "F8",
    "F9",
    "F10",
    "F11",
    "F12",
    "F13",
    "F14",
    "F15",
    "F16",
    "F17",
    "F18",
    "F19",
    "F20",
    "F21",
    "F22",
    "F23",
    "F24",
    "Numlock",
    "LeftShift",
    "RightShift",
    "LeftControl",
    "RightControl",
    "LeftAlt",
    "RightAlt",
    "BrowserBack",
    "BrowserForward",
    "BrowserRefresh",
    "BrowserStop",
    "BrowserSearch",
    "BrowserFavorites",
    "BrowserHome",
    "VolumeUp",
    "VolumeDown",
    "VolumeMute",
    "MediaNext",
    "MediaPrev",
    "MediaStop",
    "MediaPlayPause",
    "Comma",
    "Plus",
    "Minus",
    "Period",
    "OEM1",
    "OEM2",
    "OEM3",
    "OEM4",
    "OEM5",
    "OEM6",
    "OEM7",
    "OEM8",
    "OEM9"
};

#ifdef WIN32
WindowLib::KeyCode WindowLib::KeyCodeMaps::VK_To_KeyCode[] = {
    WindowLib::KeyCode::Unknown,   // OFFSET
    WindowLib::KeyCode::Unknown,   // VK_LBUTTON
    WindowLib::KeyCode::Unknown,   // VK_RBUTTON
    WindowLib::KeyCode::Attn,   // VK_CANCEL
    WindowLib::KeyCode::Unknown,   // VK_MBUTTON
    WindowLib::KeyCode::Unknown,   // VK_XBUTTON1
    WindowLib::KeyCode::Unknown,   // VK_XBUTTON2
    WindowLib::KeyCode::Unknown,   // Undefined
    WindowLib::KeyCode::Backspace, // VK_BACKSPACE
    WindowLib::KeyCode::Tab,       // VK_TAB
    WindowLib::KeyCode::Unknown,   // Reserved
    WindowLib::KeyCode::Unknown,   // Reserved
    WindowLib::KeyCode::Unknown,   // VK_CLEAR
    WindowLib::KeyCode::Enter,     // VK_RETURN
    WindowLib::KeyCode::Unknown,   // Undefined
    WindowLib::KeyCode::Unknown,   // Undefined
    WindowLib::KeyCode::Shift,     // VK_SHIFT
    WindowLib::KeyCode::Control,   // VK_CONTROL
    WindowLib::KeyCode::Alt,       // VK_MENU
    WindowLib::KeyCode::Pause,   // VK_PAUSE
    WindowLib::KeyCode::CapsLock,  // VK_CAPITAL
    WindowLib::KeyCode::Unknown,   // VK_KANA, VK_HANGUEL, VK_HANGUL
    WindowLib::KeyCode::Unknown,   // VK_IME_ON
    WindowLib::KeyCode::Unknown,   // VK_JUNJA
    WindowLib::KeyCode::Unknown,   // VK_FINAL
    WindowLib::KeyCode::Unknown,   // VK_HANJA, VK_KANJI
    WindowLib::KeyCode::Unknown,   // VK_IME_OFF
    WindowLib::KeyCode::Escape,    // VK_ESCAPE
    WindowLib::KeyCode::Unknown,   // VK_CONVERT
    WindowLib::KeyCode::Unknown,   // VK_NONCONVERT
    WindowLib::KeyCode::Unknown,   // VK_ACCEPT
    WindowLib::KeyCode::Unknown,   // VK_MODECHANGE
    WindowLib::KeyCode::Spacebar,  // VK_SPACE
    WindowLib::KeyCode::PageUp,    // VK_PRIOR
    WindowLib::KeyCode::PageDown,  // VK_NEXT
    WindowLib::KeyCode::Unknown,   // VK_END
    WindowLib::KeyCode::Unknown,   // VK_HOME
    WindowLib::KeyCode::LeftArrow, // VK_LEFT
    WindowLib::KeyCode::UpArrow,   // VK_UP
    WindowLib::KeyCode::RightArrow,    // VK_RIGHT
    WindowLib::KeyCode::DownArrow, // VK_DOWN
    WindowLib::KeyCode::Unknown,   // VK_SELECT
    WindowLib::KeyCode::Unknown,   // VK_PRINT
    WindowLib::KeyCode::Unknown,   // VK_EXECUTE
    WindowLib::KeyCode::PrintScreen,   // VK_SNAPSHOT
    WindowLib::KeyCode::Insert,    // VK_INSERT
    WindowLib::KeyCode::Delete,    // VK_DELETE
    WindowLib::KeyCode::Unknown,   // VK_HELP
    WindowLib::KeyCode::Key0,      
    WindowLib::KeyCode::Key1,
    WindowLib::KeyCode::Key2,
    WindowLib::KeyCode::Key3,
    WindowLib::KeyCode::Key4,
    WindowLib::KeyCode::Key5,
    WindowLib::KeyCode::Key6,
    WindowLib::KeyCode::Key7,
    WindowLib::KeyCode::Key8,
    WindowLib::KeyCode::Key9,
    WindowLib::KeyCode::Unknown,   // Undefined
    WindowLib::KeyCode::Unknown,   // Undefined
    WindowLib::KeyCode::Unknown,   // Undefined
    WindowLib::KeyCode::Unknown,   // Undefined
    WindowLib::KeyCode::Unknown,   // Undefined
    WindowLib::KeyCode::Unknown,   // Undefined
    WindowLib::KeyCode::Unknown,   // Undefined
    WindowLib::KeyCode::KeyA,
    WindowLib::KeyCode::KeyB,
    WindowLib::KeyCode::KeyC,
    WindowLib::KeyCode::KeyD,
    WindowLib::KeyCode::KeyE,
    WindowLib::KeyCode::KeyF,
    WindowLib::KeyCode::KeyG,
    WindowLib::KeyCode::KeyH,
    WindowLib::KeyCode::KeyI,
    WindowLib::KeyCode::KeyJ,
    WindowLib::KeyCode::KeyK,
    WindowLib::KeyCode::KeyL,
    WindowLib::KeyCode::KeyM,
    WindowLib::KeyCode::KeyN,
    WindowLib::KeyCode::KeyO,
    WindowLib::KeyCode::KeyP,
    WindowLib::KeyCode::KeyQ,
    WindowLib::KeyCode::KeyR,
    WindowLib::KeyCode::KeyS,
    WindowLib::KeyCode::KeyT,
    WindowLib::KeyCode::KeyU,
    WindowLib::KeyCode::KeyV,
    WindowLib::KeyCode::KeyW,
    WindowLib::KeyCode::KeyX,
    WindowLib::KeyCode::KeyY,
    WindowLib::KeyCode::KeyZ,
    WindowLib::KeyCode::Windows,   // VK_LWIN
    WindowLib::KeyCode::Windows,   // VK_RWIN
    WindowLib::KeyCode::Unknown,   // VK_APPS
    WindowLib::KeyCode::Unknown,   // Reserved
    WindowLib::KeyCode::Unknown,   // VK_SLEEP
    WindowLib::KeyCode::Numpad0,   // VK_NUMPAD0
    WindowLib::KeyCode::Numpad1,   // VK_NUMPAD1
    WindowLib::KeyCode::Numpad2,   // VK_NUMPAD2
    WindowLib::KeyCode::Numpad3,   // VK_NUMPAD3
    WindowLib::KeyCode::Numpad4,   // VK_NUMPAD4
    WindowLib::KeyCode::Numpad5,   // VK_NUMPAD5
    WindowLib::KeyCode::Numpad6,   // VK_NUMPAD6
    WindowLib::KeyCode::Numpad7,   // VK_NUMPAD7
    WindowLib::KeyCode::Numpad8,   // VK_NUMPAD8
    WindowLib::KeyCode::Numpad9,   // VK_NUMPAD9
    WindowLib::KeyCode::NumpadMultiply,    // VK_MULTIPLY
    WindowLib::KeyCode::NumpadAdd, // VK_ADD
    WindowLib::KeyCode::Unknown,   // VK_SEPARATOR
    WindowLib::KeyCode::NumpadSubstract,   // VK_SUBSTRACT
    WindowLib::KeyCode::NumpadDecimal, // VK_DECIMAL
    WindowLib::KeyCode::NumpadDivide,  // VK_DIVIDE
    WindowLib::KeyCode::F1,    // VK_F1
    WindowLib::KeyCode::F2,    // VK_F2
    WindowLib::KeyCode::F3,    // VK_F3
    WindowLib::KeyCode::F4,    // VK_F4
    WindowLib::KeyCode::F5,    // VK_F5
    WindowLib::KeyCode::F6,    // VK_F6
    WindowLib::KeyCode::F7,    // VK_F7
    WindowLib::KeyCode::F8,    // VK_F8
    WindowLib::KeyCode::F9,    // VK_F9
    WindowLib::KeyCode::F10,   // VK_F10
    WindowLib::KeyCode::F11,   // VK_F11
    WindowLib::KeyCode::F12,   // VK_F12
    WindowLib::KeyCode::F13,   // VK_F13
    WindowLib::KeyCode::F14,   // VK_F14
    WindowLib::KeyCode::F15,   // VK_F15
    WindowLib::KeyCode::F16,   // VK_F16
    WindowLib::KeyCode::F17,   // VK_F17
    WindowLib::KeyCode::F18,   // VK_F18
    WindowLib::KeyCode::F19,   // VK_F19
    WindowLib::KeyCode::F20,   // VK_F20
    WindowLib::KeyCode::F21,   // VK_F21
    WindowLib::KeyCode::F22,   // VK_F22
    WindowLib::KeyCode::F23,   // VK_F23
    WindowLib::KeyCode::F24,   // VK_F24
    WindowLib::KeyCode::Unknown,   // Unassigned
    WindowLib::KeyCode::Unknown,   // Unassigned
    WindowLib::KeyCode::Unknown,   // Unassigned
    WindowLib::KeyCode::Unknown,   // Unassigned
    WindowLib::KeyCode::Unknown,   // Unassigned
    WindowLib::KeyCode::Unknown,   // Unassigned
    WindowLib::KeyCode::Unknown,   // Unassigned
    WindowLib::KeyCode::Unknown,   // Unassigned
    WindowLib::KeyCode::Numlock,   // VK_NUMLOCK
    WindowLib::KeyCode::Unknown,   // VK_SCROLL
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // Unassigned
    WindowLib::KeyCode::Unknown,   // Unassigned
    WindowLib::KeyCode::Unknown,   // Unassigned
    WindowLib::KeyCode::Unknown,   // Unassigned
    WindowLib::KeyCode::Unknown,   // Unassigned
    WindowLib::KeyCode::Unknown,   // Unassigned
    WindowLib::KeyCode::Unknown,   // Unassigned
    WindowLib::KeyCode::Unknown,   // Unassigned
    WindowLib::KeyCode::Unknown,   // Unassigned
    WindowLib::KeyCode::LeftShift, // VK_LSHIFT
    WindowLib::KeyCode::RightShift,    // VK_RSHIFT
    WindowLib::KeyCode::LeftControl,   // VK_LCONTROL
    WindowLib::KeyCode::RightControl,  // VK_RCONTROL
    WindowLib::KeyCode::LeftAlt,   // VK_LMENU
    WindowLib::KeyCode::RightAlt,  // VK_RMENU
    WindowLib::KeyCode::BrowserBack,   // VK_BROWSER_BACK
    WindowLib::KeyCode::BrowserForward,   // VK_BROWSER_FORWARD
    WindowLib::KeyCode::BrowserRefresh,   // VK_BROWSER_REFRESH
    WindowLib::KeyCode::BrowserStop,   // VK_BROWSER_STOP
    WindowLib::KeyCode::BrowserSearch,   // VK_BROWSER_SEARCH
    WindowLib::KeyCode::BrowserFavorites,   // VK_BROWSER_FAVORITES
    WindowLib::KeyCode::BrowserHome,   // VK_BROWSER_HOME
    WindowLib::KeyCode::VolumeMute,   // VK_VOLUME_MUTE
    WindowLib::KeyCode::VolumeDown,   // VK_VOLUME_DOWN
    WindowLib::KeyCode::VolumeUp,   // VK_VOLUME_UP
    WindowLib::KeyCode::MediaNext,   // VK_MEDIA_NEXT_TRACK
    WindowLib::KeyCode::MediaPrev,   // VK_MEDIA_PREV_TRACK
    WindowLib::KeyCode::MediaStop,   // VK_MEDIA_STOP
    WindowLib::KeyCode::MediaPlayPause,   // VK_MEDIA_PLAY_PAUSE
    WindowLib::KeyCode::Unknown,   // VK_LAUNCH_MAIL
    WindowLib::KeyCode::Unknown,   // VK_LAUNCH_MEDIA_SELECT
    WindowLib::KeyCode::Unknown,   // VK_LAUNCH_APP1
    WindowLib::KeyCode::Unknown,   // VK_LAUNCH_APP2
    WindowLib::KeyCode::Unknown,   // Reserved
    WindowLib::KeyCode::Unknown,   // Reserved
    WindowLib::KeyCode::OEM1,   // VK_OEM_1
    WindowLib::KeyCode::Plus,   // VK_OEM_PLUS
    WindowLib::KeyCode::Comma,   // VK_OEM_COMMA
    WindowLib::KeyCode::Minus,   // VK_OEM_MINUS
    WindowLib::KeyCode::Period,   // VK_OEM_PERIOD
    WindowLib::KeyCode::OEM2,   // VK_OEM_2
    WindowLib::KeyCode::OEM3,   // VK_OEM_3
    WindowLib::KeyCode::Unknown,   // Reserved 0xC1
    WindowLib::KeyCode::Unknown,   // Reserved 0xC2
    WindowLib::KeyCode::Unknown,   // Reserved 0xC3
    WindowLib::KeyCode::Unknown,   // Reserved 0xC4
    WindowLib::KeyCode::Unknown,   // Reserved 0xC5
    WindowLib::KeyCode::Unknown,   // Reserved 0xC6
    WindowLib::KeyCode::Unknown,   // Reserved 0xC7
    WindowLib::KeyCode::Unknown,   // Reserved 0xC8
    WindowLib::KeyCode::Unknown,   // Reserved 0xC9
    WindowLib::KeyCode::Unknown,   // Reserved 0xCA
    WindowLib::KeyCode::Unknown,   // Reserved 0xCB
    WindowLib::KeyCode::Unknown,   // Reserved 0xCC
    WindowLib::KeyCode::Unknown,   // Reserved 0xCD
    WindowLib::KeyCode::Unknown,   // Reserved 0xCE
    WindowLib::KeyCode::Unknown,   // Reserved 0xCF
    WindowLib::KeyCode::Unknown,   // Reserved 0xD0
    WindowLib::KeyCode::Unknown,   // Reserved 0xD1
    WindowLib::KeyCode::Unknown,   // Reserved 0xD2
    WindowLib::KeyCode::Unknown,   // Reserved 0xD3
    WindowLib::KeyCode::Unknown,   // Reserved 0xD4
    WindowLib::KeyCode::Unknown,   // Reserved 0xD5
    WindowLib::KeyCode::Unknown,   // Reserved 0xD6
    WindowLib::KeyCode::Unknown,   // Reserved 0xD7
    WindowLib::KeyCode::Unknown,   // Unassigned 0xD8
    WindowLib::KeyCode::Unknown,   // Unassigned 0xD9
    WindowLib::KeyCode::Unknown,   // Unassigned 0xDA
    WindowLib::KeyCode::OEM4,   // VK_OEM_4
    WindowLib::KeyCode::OEM5,   // VK_OEM_5
    WindowLib::KeyCode::OEM6,   // VK_OEM_6
    WindowLib::KeyCode::OEM7,   // VK_OEM_7
    WindowLib::KeyCode::OEM8,   // VK_OEM_8
    WindowLib::KeyCode::Unknown,   // Reserved
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::OEM9,   // VK_OEM_102
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // VK_PROCESSKEY
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // VK_PACKET
    WindowLib::KeyCode::Unknown,   // Unassigned
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // OEM specific
    WindowLib::KeyCode::Unknown,   // VK_ATTN
    WindowLib::KeyCode::Unknown,   // VK_CRSEL
    WindowLib::KeyCode::Unknown,   // VK_EXSEL
    WindowLib::KeyCode::Unknown,   // VK_EREOF
    WindowLib::KeyCode::Unknown,   // VK_PLAY
    WindowLib::KeyCode::Unknown,   // VK_ZOOM
    WindowLib::KeyCode::Unknown,   // VK_NONAME
    WindowLib::KeyCode::Unknown,   // VK_PA1
    WindowLib::KeyCode::Unknown,   // VK_OEM_CLEAR
};
#endif


const char* WindowLib::KeyCodeMaps::KeyCode_To_String(KeyCode code) {
    return KeyCodeInt_To_String[(int)code];
}