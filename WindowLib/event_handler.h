#ifndef __EVENTLIB_EVENT_HANDLER_INCLUDED_
#define __EVENTLIB_EVENT_HANDLER_INCLUDED_

#ifdef BUILD_DLL
#define DLL __declspec(dllexport)
#else
#define DLL __declspec(dllimport)
#endif


#include <exception>
#include <list>
#include <functional>
#include "event.h"

#define INITIALIZATION_EXCEPTION std::exception("The initialization of EventHandler failed");

namespace EventLib {
    /**
     * @brief Handler for one event
     * @tparam Type of event fired
     * 
     * This class allows to fire events and send them to multiple registered
     * listeners. The events can be canceled and have a complete traceback to
     * the sender.
     * 
     * (It is recommended to use this calss as private field)
     */
    template<typename T>
    class DLL EventHandler {
    public:
        /**
         * @brief Construct a new Event Handler object
         * @param owner (Optionnal) The object owner of the handler
         */
        EventHandler(void* owner = nullptr);
        /**
         * @brief Copy and construct a new Event Handler object
         * @param other Another EventHandler
         */
        EventHandler(const EventHandler& other);
        /**
         * @brief Destroy the Event Handler object
         */
        ~EventHandler();

        /**
         * @brief Fires an event
         * @param event Event to fire
         * @param is_canceled (Optionnal) [out] If specified, it returns wheter
         * the event has been canceled. It turns the event into a cancelable one
         * and the Event::Cancel function becomes available.
         * \return true if all listeners were correctly triggered
         */ 
        bool Fire(Event<T> event, bool* is_canceled = nullptr);

        /**
         * \brief Adds a listener to the event
         * \param callback Event callback
         * \return true if listener is registered, false otherwise
         */
        bool Listen(std::function<void(Event<T>)> callback);

        /**
         * @brief Gets the number of listeners to the event
         * @return NUmer of listeners (or -1 if an error occured)
         */
        int NbListeners() const;

        /**
         * \brief Alias for Listen function
         * \param callback Event callback
         */
        EventHandler& operator+=(std::function<void(Event<T>)> callback) {
            Listen(callback);
            return *this;
        }

    private:
        void* _owner;   /** Pointer to the object containing the handler */
        // std::list<void(*)(Event<T>)>* _listeners;   /** List of listeners */
        std::list<std::function<void(Event<T>)>>* _listeners;
    };

    template<typename T>
    //
    EventHandler<T>::EventHandler(void* owner): _owner(owner),// _listeners(new std::list<void(*)(Event<T>)>()) {
                    _listeners(new std::list<std::function<void(Event<T>)>>()) {
        // if the lsit was not correctly initializad, throw an error
        if (!_listeners)
            throw INITIALIZATION_EXCEPTION
    }

    template <typename T>
    EventHandler<T>::EventHandler(const EventHandler& other): _owner(other._owner), _listeners(nullptr) {
        if (other._listeners)
            _listeners = new std::list<std::function<void(Event<T>)>>(*other._listeners);
        else
            _listeners = new std::list<std::function<void(Event<T>)>>();
    }

    template<typename T>
    EventHandler<T>::~EventHandler() {
        if (!_listeners) {
            delete _listeners;
            _listeners = nullptr;
        }
    }

    template<typename T>
    bool EventHandler<T>::Fire(Event<T> event, bool* is_canceled) {
        if (!_listeners) return false;

        event._handler = this;  // Link the handler

        // if the event is cancelable, pass the cancel field
        if (is_canceled)
            event.__canceled = is_canceled;

        // trigger all listeners
        for (auto it = _listeners->begin(); it != _listeners->end(); it++)
            if (*it) (*it)(event);

        return true;
    }

    template<typename T>
    bool EventHandler<T>::Listen(std::function<void(Event<T>)> callback) {
        if (!callback || !_listeners) return false;
        _listeners->push_back(callback);
        return true;
    }

    template<typename T>
    int EventHandler<T>::NbListeners() const {
        if (!_listeners) return -1;
        return _listeners->size();
    }


    /**
     * @brief Linked handler to a EventHandler
     * @tparam Type of event fired by the linked handler
     * 
     * This class is linked to a EventHandler and only allows "readonly" actions,
     * such as adding listeners and receiving events.
     * It should be used as public field.
     */
    template<typename T>
    class DLL RestrictedEventHandler {
    public:
        /**
         * @brief Construct a new Restricted Event Handler object
         * @param handler Main (or linked) EventHandler
         */
        RestrictedEventHandler(EventHandler<T>& handler);
        /**
         * @brief Copy and construct a new Restricted Event Handler object
         * @param other RestrictedEventHandler tp copy
         * 
         * It basically creates a new RestrictedEventHandler
         * with the same linked EventHandler
         */
        RestrictedEventHandler(const RestrictedEventHandler& other);
        /**
         * @brief Destroy the Restricted Event Handler object
         */
        ~RestrictedEventHandler();

        /**
         * @brief Adds a listener to the linked EventHandler
         * @param callback Event callback
         */
        void Listen(std::function<void(Event<T>)> callback);
        
        /**
         * @brief Alias for RestrictedEventHandler::Listen
         * @param callback Event callback
         */
        RestrictedEventHandler& operator+=(std::function<void(Event<T>)> callback) {
            Listen(callback);
            return *this;
        }

    private:
        EventHandler<T>& _handler; /** Linked EventHandler */
    };

    template <typename T>
    RestrictedEventHandler<T>::RestrictedEventHandler(EventHandler<T>& handler): _handler(handler) {}

    template <typename T>
    RestrictedEventHandler<T>::RestrictedEventHandler(const RestrictedEventHandler& other): _handler(other._handler) {}

    template <typename T>
    RestrictedEventHandler<T>::~RestrictedEventHandler() {}

    template <typename T>
    void RestrictedEventHandler<T>::Listen(std::function<void(Event<T>)> callback) {
        _handler.Listen(callback);
    }
}

#endif // __EVENT_HANDLER_INCLUDED_
