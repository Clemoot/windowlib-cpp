#include "window.h"

WindowLib::Window::Window(std::string title): 
    BasicWindow(title),
    OnCreate(_OnCreate),
    OnDestroy(_OnDestroy),
    OnMoved(_OnMoved),
    OnResized(_OnResized),
    OnMinimized(_OnMinimized),
    OnMaximized(_OnMaximized),
    OnRestored(_OnRestored),
    OnFocusChanged(_OnFocusChanged),
    OnClose(_OnClose),
    OnQuit(_OnQuit),
    OnRender(_OnRender),
    OnKeyDown(_OnKeyDown),
    OnKeyUp(_OnKeyUp),
    OnMouseMove(_OnMouseMove),
    OnMouseButtonDown(_OnMouseButtonDown),
    OnMouseButtonUp(_OnMouseButtonUp),
    OnMouseWheel(_OnMouseWheel)
    {}

#ifdef WIN32
#define EVENT_CALLBACK(function, event_type) void WindowLib::Window::CB_##function(event_type info) { _##function.Fire(EventLib::Event<event_type>(this, info)); }
#define EVENT_CALLBACK_WITHOUT_PARAM(function) void WindowLib::Window::CB_##function() { _##function.Fire(EventLib::Event<int>(this, 0)); }

EVENT_CALLBACK(OnCreate, Events::OnCreateEvent)
void WindowLib::Window::CB_OnDestroy() {
    PostQuitMessage(0);
    _OnDestroy.Fire(EventLib::Event<int>(this, 0));
}
EVENT_CALLBACK(OnMoved, Events::OnWindowChangedEvent)
EVENT_CALLBACK(OnResized, Events::OnWindowChangedEvent)
EVENT_CALLBACK(OnMinimized, Events::OnWindowChangedEvent)
EVENT_CALLBACK(OnMaximized, Events::OnWindowChangedEvent)
EVENT_CALLBACK(OnRestored, Events::OnWindowChangedEvent)
EVENT_CALLBACK(OnFocusChanged, Events::OnWindowFocusChangedEvent)
void WindowLib::Window::CB_OnClose() {
    bool cancel = false;
    _OnClose.Fire(EventLib::Event<int>(this, 0), &cancel);

    if (!cancel)
        Destroy();
}
EVENT_CALLBACK(OnQuit, int)
EVENT_CALLBACK_WITHOUT_PARAM(OnRender)
EVENT_CALLBACK(OnKeyDown, Events::OnKeyStateChangedEvent)
EVENT_CALLBACK(OnKeyUp, Events::OnKeyStateChangedEvent)
EVENT_CALLBACK(OnMouseMove, Events::OnMouseMoveEvent)
EVENT_CALLBACK(OnMouseButtonDown, Events::OnMouseButtonChangedEvent)
EVENT_CALLBACK(OnMouseButtonUp, Events::OnMouseButtonChangedEvent)
EVENT_CALLBACK(OnMouseWheel, Events::OnMouseWheelEvent)

#undef EVENT_CALLBACK

#endif