#ifndef __ENVIRONMENT_INCLUDED
#define __ENVIRONMENT_INCLUDED

#ifdef BUILD_DLL
#define DLL __declspec(dllexport)
#else
#define DLL __declspec(dllimport)
#endif

namespace WindowLib {
    /**
     * @brief Stores all information about operating system or desktop environment
     * The class is a singleton. Only Iniatilze and Destroy members need to be called.
     */
    class DLL Environment {
    public:
        struct Rect {   //!< Structure for positioning
            int x;  //!< X-coordinate
            int y;  //!< Y-coordinate
            unsigned int w; //!< Width
            unsigned int h; //!< Height
        };

        /**
         * @brief Initializes the Environment instance
         * @return true if the instance is initialized, false otherwise
         */
        static bool Initialize();
        static void Destroy();  //!< Destroy the Environment instance

        /**
         * @brief Gets taskba size
         * @return Rect 
         */
        static Rect TaskbarInfo();

        static void RefreshEnvironment(); //!< Refreshes all environment information

    private:
        static Environment* _Instance;  //!< Environment instance
        Rect _taskbar;  //!< Taskbar position and size

        Environment(); //!< Construct a new Environment object

        void _RefreshEnvironment(); //!< Called by RefreshEnvironment
    };
}

#endif // __ENVIRONMENT_INCLUDED
