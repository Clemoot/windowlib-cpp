#include "basic_window.h"
#include "errors.h"
#include "environment.h"
#include "keycodes.h"

#ifdef WIN32
WindowLib::BasicWindow* WindowLib::BasicWindow::WindowInCreation = nullptr;
const char* WindowLib::BasicWindow::WINDOW_CLASS_NAME = "WindowLibClass";
#endif

WindowLib::BasicWindow::BasicWindow(std::string title): //_title(std::unique_ptr<std::string>(new std::string(title))) {
    _isClosed(true)
{
    char* t = new char[title.length()];
    for (unsigned int i = 0; i < title.length(); i++)
        t[i] = title[i];
    _title = t;
}

WindowLib::BasicWindow::BasicWindow(const BasicWindow& window) {
    if (window._title) {
        int len = 0;
        for (len = 0; window._title[len] != 0; len++);
        char* t = new char[len];
        for (int i = 0; i < len; i++)
            t[i] = window._title[i];
        _title = t;
    } else
        _title = nullptr;
}

WindowLib::BasicWindow::~BasicWindow() {
    if (_title) {
        delete _title;
        _title = nullptr;
    }

    if (_handle) {
        DestroyWindow((HWND)_handle);
        _handle = nullptr;
    }
}

bool WindowLib::BasicWindow::Create(int width, int height, long style) {
    #ifdef WIN32
    while (BasicWindow::WindowInCreation);

    BasicWindow::WindowInCreation = this;

    _hInstance = 0;
    WNDCLASSA wc = {};
    wc.lpfnWndProc = __WindowProc;
    wc.hInstance = _hInstance;
    wc.lpszClassName = WINDOW_CLASS_NAME;
    wc.cbClsExtra = sizeof(LONG_PTR);

    RegisterClassA(&wc);

    // WS_CAPTION : TItle bar (only window name)
    // WS_SIZEBOX : Resizable
    // WS_SYSMENU : Top-right corner buttons
    // WS_MAXIMIZEBOX : Toggle maximize button
    // WS_MINIMIZEBOX : Toggle minimize button

    long w_style = 0;
    if (style & TITLEBAR) {
        w_style |= WS_CAPTION;
        std::cout << "Titlebar" << std::endl;
    }
    if (style & RESIZABLE) {
        std::cout << "Resizable" << std::endl;
        w_style |= WS_SIZEBOX;
    }
    if (style & CLOSE_BUTTON) {
        std::cout << "Close button" << std::endl;
        w_style |= WS_SYSMENU;
    }
    if (style & MAXIMIZE_BUTTON) {
        std::cout << "Maximize button" << std::endl;
        w_style |= WS_SYSMENU | WS_MAXIMIZEBOX;
    }
    if (style & MINIMIZE_BUTTON) {
        std::cout << "Minimize button" << std::endl;
        w_style |= WS_SYSMENU | WS_MINIMIZEBOX;
    }

    long ex_style = 0;
    if (style & ALWAYS_ON_TOP) ex_style |= WS_EX_TOPMOST;

    _handle = CreateWindowExA(
        ex_style,
        WINDOW_CLASS_NAME,
        _title,
        w_style,
        CW_USEDEFAULT, CW_USEDEFAULT, width, height,
        0,
        0,
        _hInstance,
        0
    );
    #endif

    if (!_handle)
        return false;

    #ifdef WIN32
    SetWindowLongPtrA((HWND)_handle, GWLP_USERDATA, (LONG_PTR)this);
    BasicWindow::WindowInCreation = nullptr;
    #endif

    _isClosed = false;

    return true;
}

bool WindowLib::BasicWindow::Show() const {
    if (!_handle) return false;

    #ifdef WIN32
    ShowWindow((HWND)_handle, SW_SHOW);
    return true;
    #endif
}

bool WindowLib::BasicWindow::Hide() const {
    if (!_handle) return false;

    #ifdef WIN32
    ShowWindow((HWND)_handle, SW_HIDE);
    return true;
    #endif
}

bool WindowLib::BasicWindow::Destroy() {
    if (!_handle) return false;

    #ifdef WIN32
    if (!DestroyWindow((HWND)_handle))
        return false;
    #endif

    _handle = nullptr;
    _isClosed = true;

    return true;
}

bool WindowLib::BasicWindow::Minimize() {
    if (!_handle) return false;

    #ifdef WIN32
    ShowWindow((HWND)_handle, SW_MINIMIZE);
    return true;
    #endif
}

bool WindowLib::BasicWindow::Maximize() {
    if (!_handle) return false;

    #ifdef WIN32
    ShowWindow((HWND)_handle, SW_MAXIMIZE);
    return true;
    #endif
}

bool WindowLib::BasicWindow::Restore() {
    if (!_handle) return false;

    #ifdef WIN32
    ShowWindow((HWND)_handle, SW_RESTORE);
    return true;
    #endif
}

void WindowLib::BasicWindow::WaitMessages() {
    if (!_handle) return;
    #ifdef WIN32
    MSG msg = {};
    while (GetMessage(&msg, nullptr, 0, 0)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }
    if (msg.message == WM_QUIT) {
        try {
            CB_OnQuit(msg.wParam);
        } catch (Exceptions::NotImplementedException e) {}
    }
    #endif
}

void WindowLib::BasicWindow::PollMessages() {
    if (!_handle) return;
    #ifdef WIN32
    MSG msg = {};
    while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
        if (msg.message == WM_QUIT) {
            try {
                CB_OnQuit(msg.wParam);
            } catch (Exceptions::NotImplementedException e) {}
        } else {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }
    #endif
}

const char* WindowLib::BasicWindow::WindowTitle() const {
    return _title;
}

bool WindowLib::BasicWindow::WindowTitle(const char* title) {
    if (!_handle) return false;
    #ifdef WIN32
        if (SetWindowTextA((HWND)_handle, title)) {
            _title = title;
            return true;
        }
        return false;
    #endif
}

void* WindowLib::BasicWindow::Handle() {
    return _handle;
}

WindowLib::Rect WindowLib::BasicWindow::Pos() const {
    return _pos;
}

bool WindowLib::BasicWindow::Pos(Rect rect) {
    if (!_handle) return false;

    #ifdef WIN32
    if (SetWindowPos((HWND)_handle, HWND_TOPMOST, rect.x, rect.y, rect.w, rect.h, SWP_NOZORDER)) {
        _pos = rect;
        return true;
    }
    return false;
    #endif
}

bool WindowLib::BasicWindow::IsClosed() const {
    return _isClosed;
}

#define NOT_IMPLEMENTED_EVENT_CALLBACK(function, params) void WindowLib::BasicWindow::CB_##function(params) { throw Exceptions::NotImplementedException(#function); }

NOT_IMPLEMENTED_EVENT_CALLBACK(OnCreate, Events::OnCreateEvent info)
NOT_IMPLEMENTED_EVENT_CALLBACK(OnDestroy, void)
NOT_IMPLEMENTED_EVENT_CALLBACK(OnMoved, Events::OnWindowChangedEvent info)
NOT_IMPLEMENTED_EVENT_CALLBACK(OnResized, Events::OnWindowChangedEvent info)
NOT_IMPLEMENTED_EVENT_CALLBACK(OnMaximized, Events::OnWindowChangedEvent info)
NOT_IMPLEMENTED_EVENT_CALLBACK(OnMinimized, Events::OnWindowChangedEvent info)
NOT_IMPLEMENTED_EVENT_CALLBACK(OnRestored, Events::OnWindowChangedEvent info)
NOT_IMPLEMENTED_EVENT_CALLBACK(OnFocusChanged, Events::OnWindowFocusChangedEvent info)
NOT_IMPLEMENTED_EVENT_CALLBACK(OnClose, void)
void WindowLib::BasicWindow::CB_OnQuit(int quitCode) {
    _isClosed = true;
}
NOT_IMPLEMENTED_EVENT_CALLBACK(OnRender, void)
NOT_IMPLEMENTED_EVENT_CALLBACK(OnKeyDown, Events::OnKeyStateChangedEvent info)
NOT_IMPLEMENTED_EVENT_CALLBACK(OnKeyUp, Events::OnKeyStateChangedEvent info)
NOT_IMPLEMENTED_EVENT_CALLBACK(OnMouseMove, Events::OnMouseMoveEvent info)
NOT_IMPLEMENTED_EVENT_CALLBACK(OnMouseButtonDown, Events::OnMouseButtonChangedEvent info)
NOT_IMPLEMENTED_EVENT_CALLBACK(OnMouseButtonUp, Events::OnMouseButtonChangedEvent info)
NOT_IMPLEMENTED_EVENT_CALLBACK(OnMouseWheel, Events::OnMouseWheelEvent info)

#undef NOT_IMPLEMENTED_EVENT_FUNCTION

#ifdef WIN32
#define EVENT_HANDLER_IMPLEMENTATION(function) long int WindowLib::BasicWindow::EH_ ## function(HWND hWnd, unsigned int wParam, long int lParam) 

#define BEGIN_TRY_CLAUSE try {
#define END_TRY_CLAUSE } catch (Exceptions::NotImplementedException e) {}
#define END_TRY_CLAUSE_WITH } catch (Exceptions::NotImplementedException e)

EVENT_HANDLER_IMPLEMENTATION(OnCreate) {
    CREATESTRUCTA cs = *(LPCREATESTRUCTA)lParam;
    _hInstance = cs.hInstance;
    Events::OnCreateEvent info = { 
        cs.hInstance,
        cs.hwndParent,
        {
            cs.x,
            cs.y,
            cs.cx,
            cs.cy
        },
        std::string(cs.lpszName)
    };
    _pos = info.pos;

    BEGIN_TRY_CLAUSE
        CB_OnCreate(info);
    END_TRY_CLAUSE
    return 0;
}
EVENT_HANDLER_IMPLEMENTATION(OnDestroy) {
    BEGIN_TRY_CLAUSE
        CB_OnDestroy();
    END_TRY_CLAUSE_WITH {
        PostQuitMessage(0);
    }
    return 0;
}
EVENT_HANDLER_IMPLEMENTATION(OnActivate) {
    _isFocused = wParam == WA_INACTIVE ? false : true;
    Events::OnWindowFocusChangedEvent info = {
        _isFocused,
        (void*)lParam
    };
    BEGIN_TRY_CLAUSE
        CB_OnFocusChanged(info);
    END_TRY_CLAUSE
    return 0;
}
EVENT_HANDLER_IMPLEMENTATION(OnClose) {
    BEGIN_TRY_CLAUSE
        CB_OnClose();
    END_TRY_CLAUSE_WITH {
        Destroy();
    }
    return 0;
}
EVENT_HANDLER_IMPLEMENTATION(OnQuit) {
    BEGIN_TRY_CLAUSE
        CB_OnQuit(wParam);
    END_TRY_CLAUSE
    return 0;
}
EVENT_HANDLER_IMPLEMENTATION(OnRender) { 
    BEGIN_TRY_CLAUSE
        CB_OnRender();
    END_TRY_CLAUSE
    return 0;
}
EVENT_HANDLER_IMPLEMENTATION(OnKeyDown) { 
    unsigned char state[256];
    GetKeyboardState(state);

    wchar_t c = 0;
    ToUnicode(wParam, (lParam & 0xFF0000) >> 16, state, &c, 1, 0);

    Events::OnKeyStateChangedEvent info = {
        KeyCodeMaps::VK_To_KeyCode[wParam],
        (char)c,
        true
    };

    BEGIN_TRY_CLAUSE
        CB_OnKeyDown(info);
    END_TRY_CLAUSE

    return 0;
}
EVENT_HANDLER_IMPLEMENTATION(OnKeyUp) { 
    unsigned char state[256];
    GetKeyboardState(state);

    wchar_t c = 0;
    ToUnicode(wParam, (lParam & 0xFF0000) >> 16, state, &c, 1, 0);

    Events::OnKeyStateChangedEvent info = {
        KeyCodeMaps::VK_To_KeyCode[wParam],
        (char)c,
        false
    };

    BEGIN_TRY_CLAUSE
        CB_OnKeyUp(info);
    END_TRY_CLAUSE

    return 0;
}
EVENT_HANDLER_IMPLEMENTATION(OnMouseMove) {
    Events::OnMouseMoveEvent info = {
        lParam & 0xFFFF,
        lParam >> 16 
    };

    BEGIN_TRY_CLAUSE
        CB_OnMouseMove(info);
    END_TRY_CLAUSE

    return 0;
}
EVENT_HANDLER_IMPLEMENTATION(OnMouseLeftButtonDown) {
    Events::OnMouseButtonChangedEvent info = {
        MouseButton::LeftButton,
        true,
        lParam & 0xFFFF,
        lParam >> 16
    };

    BEGIN_TRY_CLAUSE
        CB_OnMouseButtonDown(info);
    END_TRY_CLAUSE
    return 0;
}
EVENT_HANDLER_IMPLEMENTATION(OnMouseLeftButtonUp) {
    Events::OnMouseButtonChangedEvent info = {
        MouseButton::LeftButton,
        false,
        lParam & 0xFFFF,
        lParam >> 16
    };

    BEGIN_TRY_CLAUSE
        CB_OnMouseButtonUp(info);
    END_TRY_CLAUSE
    return 0;
}
EVENT_HANDLER_IMPLEMENTATION(OnMouseRightButtonDown) {
    Events::OnMouseButtonChangedEvent info = {
        MouseButton::RightButton,
        true,
        lParam & 0xFFFF,
        lParam >> 16
    };

    BEGIN_TRY_CLAUSE
        CB_OnMouseButtonDown(info);
    END_TRY_CLAUSE
    return 0;
}
EVENT_HANDLER_IMPLEMENTATION(OnMouseRightButtonUp) {
    Events::OnMouseButtonChangedEvent info = {
        MouseButton::LeftButton,
        false,
        lParam & 0xFFFF,
        lParam >> 16
    };

    BEGIN_TRY_CLAUSE
        CB_OnMouseButtonUp(info);
    END_TRY_CLAUSE
    return 0;
}
EVENT_HANDLER_IMPLEMENTATION(OnMouseWheel) {
    Events::OnMouseWheelEvent info = {
        Events::OnMouseWheelEvent::Vertical,
        (int)wParam >> 16,
        lParam & 0xFFFF,
        lParam >> 16
    };

    BEGIN_TRY_CLAUSE
        CB_OnMouseWheel(info);
    END_TRY_CLAUSE

    return 0;
}
EVENT_HANDLER_IMPLEMENTATION(OnMouseWheelHorizontal) {
    Events::OnMouseWheelEvent info = {
        Events::OnMouseWheelEvent::Horizontal,
        (int)wParam >> 16,
        lParam & 0xFFFF,
        lParam >> 16
    };

    BEGIN_TRY_CLAUSE
        CB_OnMouseWheel(info);
    END_TRY_CLAUSE

    return 0;
}
EVENT_HANDLER_IMPLEMENTATION(OnWindowPosChanged) {
    WINDOWPOS wp = *(WINDOWPOS*)lParam;

    Events::OnWindowChangedEvent info = {
        wp.x, wp.y,
        wp.cx, wp.cy
    };
 
    if (!(wp.flags & SWP_NOSIZE)) {
        _pos.w = wp.cx; _pos.h = wp.cy;
        BEGIN_TRY_CLAUSE
            CB_OnResized(info);
        END_TRY_CLAUSE
        
        auto tbSize = Environment::TaskbarInfo();
        int max_w = _max_w - (tbSize.w < tbSize.h ? tbSize.w : 0);
        int max_h = _max_h - (tbSize.w > tbSize.h ? tbSize.h : 0);
        if (info.w == max_w && info.h == max_h)
            BEGIN_TRY_CLAUSE
                CB_OnMaximized(info);
            END_TRY_CLAUSE                
    }
    if (!(wp.flags & SWP_NOMOVE)) {
        int old_x = _pos.x, old_y = _pos.y;
        _pos.x = wp.x; _pos.y = wp.y;
        BEGIN_TRY_CLAUSE
            CB_OnMoved(info);
        END_TRY_CLAUSE
        if (info.x == -32000 && info.y == -32000)
            BEGIN_TRY_CLAUSE
                CB_OnMinimized(info);
            END_TRY_CLAUSE
        else if (old_x == -32000 && old_y == -32000 && _pos.x != 32000 && _pos.y != 32000)
            BEGIN_TRY_CLAUSE
                CB_OnRestored(info);
            END_TRY_CLAUSE
    }

    return 0;
}
EVENT_HANDLER_IMPLEMENTATION(OnMinMaxInfo) {
    MINMAXINFO mmi = *(MINMAXINFO*)lParam;
    _max_w = mmi.ptMaxSize.x;
    _max_h = mmi.ptMaxSize.y;
    return 0;
}

#undef EVENT_HANDLER_IMPLEMENTATION
#undef BEGIN_TRY_CLAUSE
#undef END_TRY_CLAUSE
#undef END_TRY_CLAUSE_WITH

long __stdcall WindowLib::BasicWindow::__WindowProc(HWND hWnd, unsigned int msg, unsigned int wParam, long int lParam) {
    BasicWindow* window = (BasicWindow*)GetWindowLongPtrA(hWnd, GWLP_USERDATA);
    if (!window) {
        if (BasicWindow::WindowInCreation)
            window = BasicWindow::WindowInCreation;
        else return DefWindowProc(hWnd, msg, wParam, lParam);    
    }
    return window->_WindowProc(hWnd, msg, wParam, lParam);
}

#define PRECALLBACK_CASE(enum, callback) case enum: res = EH_##callback(hWnd, wParam, lParam); break; 
long __stdcall WindowLib::BasicWindow::_WindowProc(HWND hWnd, unsigned int msg, unsigned int wParam, long int lParam) {
    long int res = 0;
    switch (msg) {
        #define WM_MOUSEHWHEEL 0x020E

        PRECALLBACK_CASE(WM_CREATE, OnCreate)
        PRECALLBACK_CASE(WM_DESTROY, OnDestroy)
        PRECALLBACK_CASE(WM_ACTIVATE, OnActivate)
        PRECALLBACK_CASE(WM_CLOSE, OnClose)
        PRECALLBACK_CASE(WM_QUIT, OnQuit)
        PRECALLBACK_CASE(WM_PAINT, OnRender)
        PRECALLBACK_CASE(WM_KEYDOWN, OnKeyDown)
        PRECALLBACK_CASE(WM_KEYUP, OnKeyUp)
        PRECALLBACK_CASE(WM_MOUSEMOVE, OnMouseMove)
        PRECALLBACK_CASE(WM_LBUTTONDOWN, OnMouseLeftButtonDown)
        PRECALLBACK_CASE(WM_LBUTTONUP, OnMouseLeftButtonUp)
        PRECALLBACK_CASE(WM_RBUTTONDOWN, OnMouseRightButtonDown)
        PRECALLBACK_CASE(WM_RBUTTONUP, OnMouseRightButtonUp)
        PRECALLBACK_CASE(WM_MOUSEWHEEL, OnMouseWheel)
        PRECALLBACK_CASE(WM_MOUSEHWHEEL, OnMouseWheelHorizontal)
        PRECALLBACK_CASE(WM_WINDOWPOSCHANGED, OnWindowPosChanged)
        PRECALLBACK_CASE(WM_GETMINMAXINFO, OnMinMaxInfo);
        default:
            res = DefWindowProc(hWnd, msg, wParam, lParam);
    }
    return res;
}
#undef PRECALLBACK_CASE
#endif