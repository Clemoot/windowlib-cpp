#include "environment.h"
#include <windows.h>

WindowLib::Environment* WindowLib::Environment::_Instance = nullptr;

/**
 * \brief Initializes the environment
 * \return true if en error occured, false otherwise
*/
bool WindowLib::Environment::Initialize() {
    if (_Instance) Destroy();

    _Instance = new Environment();
    _Instance->_RefreshEnvironment();
    return false;
}

void WindowLib::Environment::Destroy() {
    if (!_Instance) return;

    delete _Instance;
    _Instance = nullptr;
}

/**
 * \brief Retrieves the position and size of the taskbar
 * If the instance is not initialized yet, it initializes it
 * \return Rectangle
 */
WindowLib::Environment::Rect WindowLib::Environment::TaskbarInfo() {
    if (!_Instance) Initialize();
    return _Instance->_taskbar;
}

/**
 * \brief Refreshes all data of the environment
 * If not initialized, does nothing
 */
void WindowLib::Environment::RefreshEnvironment() {
    if (!_Instance) return;
    _Instance->_RefreshEnvironment();
}



WindowLib::Environment::Environment(): _taskbar{0, 0, 0, 0} {}

void WindowLib::Environment::_RefreshEnvironment() {
    #ifdef WIN32
    // Retrieve taskbar info
    APPBARDATA abd = {};
    if (SHAppBarMessage(ABM_GETTASKBARPOS, &abd)) {
        _taskbar.x = abd.rc.left;
        _taskbar.y = abd.rc.top;
        _taskbar.w = abd.rc.right - abd.rc.left;
        _taskbar.h = abd.rc.bottom - abd.rc.top;
    }
    #endif
}