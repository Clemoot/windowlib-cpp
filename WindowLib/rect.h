#ifndef __RECT_INCLUDED
#define __RECT_INCLUDED

#ifdef BUILD_DLL
#define DLL __declspec(dllexport)
#else
#define DLL __declspec(dllimport)
#endif

namespace WindowLib {
    struct DLL Rect  {  //!< Rectangle (position and size)
        int x; //!< X-coordinate
        int y; //!< Y-coordinate
        int w; //!< Width
        int h; //!< Height
    };
}

#endif // __RECT_INCLUDED
