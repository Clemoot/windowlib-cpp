#ifndef  __ERRORS_INCLUDED
#define __ERRORS_INCLUDED

#ifdef BUILD_DLL
#define DLL __declspec(dllexport)
#else
#define DLL __declspec(dllimport)
#endif

#include <ostream>
#include <exception>

namespace WindowLib {
    namespace Exceptions {
        class NotImplementedException : public std::exception {  //!< Exception when the evevnt callback is not implemented
        public:
            /** @brief Constructs a new NotImplementedException object */
            NotImplementedException(std::string function): std::exception(("La méthode " + function + " n'est pas encore implémentée !").c_str()) {}
        };
    }
}

#endif