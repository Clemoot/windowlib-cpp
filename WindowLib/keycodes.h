#ifndef __KEYCODES_INCLUDED
#define __KEYCODES_INCLUDED

#ifdef BUILD_DLL
#define DLL __declspec(dllexport)
#else
#define DLL __declspec(dllimport)
#endif

namespace WindowLib {
    enum class MouseButton {
        LeftButton,
        RightButton
    };

    enum class KeyCode {
        Unknown,
        Backspace,
        Tab,
        Enter,
        Shift,
        Control,
        Alt,
        CapsLock,
        Escape,
        Spacebar,
        PageUp,
        PageDown,
        LeftArrow,
        RightArrow,
        UpArrow,
        DownArrow,
        PrintScreen,
        Insert,
        Delete,
        Attn,
        Pause,
        Key0,
        Key1,
        Key2,
        Key3,
        Key4,
        Key5,
        Key6,
        Key7,
        Key8,
        Key9,
        KeyA,
        KeyB,
        KeyC,
        KeyD,
        KeyE,
        KeyF,
        KeyG,
        KeyH,
        KeyI,
        KeyJ,
        KeyK,
        KeyL,
        KeyM,
        KeyN,
        KeyO,
        KeyP,
        KeyQ,
        KeyR,
        KeyS,
        KeyT,
        KeyU,
        KeyV,
        KeyW,
        KeyX,
        KeyY,
        KeyZ,
        Windows,
        Numpad0,
        Numpad1,
        Numpad2,
        Numpad3,
        Numpad4,
        Numpad5,
        Numpad6,
        Numpad7,
        Numpad8,
        Numpad9,
        NumpadMultiply,
        NumpadAdd,
        NumpadSubstract,
        NumpadDivide,
        NumpadDecimal,
        F1,
        F2,
        F3,
        F4,
        F5,
        F6,
        F7,
        F8,
        F9,
        F10,
        F11,
        F12,
        F13,
        F14,
        F15,
        F16,
        F17,
        F18,
        F19,
        F20,
        F21,
        F22,
        F23,
        F24,
        Numlock,
        LeftShift,
        RightShift,
        LeftControl,
        RightControl,
        LeftAlt,
        RightAlt,
        BrowserBack,
        BrowserForward,
        BrowserRefresh,
        BrowserStop,
        BrowserSearch,
        BrowserFavorites,
        BrowserHome,
        VolumeUp,
        VolumeDown,
        VolumeMute,
        MediaNext,
        MediaPrev,
        MediaStop,
        MediaPlayPause,
        Comma,
        Plus,
        Minus,
        Period,
        OEM1,
        OEM2,
        OEM3,
        OEM4,
        OEM5,
        OEM6,
        OEM7,
        OEM8,
        OEM9,
    };

    namespace KeyCodeMaps {
        const char* KeyCode_To_String(KeyCode code);

        extern const char* KeyCodeInt_To_String[];

    #ifdef WIN32
        extern KeyCode VK_To_KeyCode[];
    #endif
    }
}

#endif // __KEYCODES_INCLUDED
