#ifndef __EVENTLIB_EVENT_INCLUDED
#define __EVENTLIB_EVENT_INCLUDED

#ifdef BUILD_DLL
#define DLL __declspec(dllexport)
#else
#define DLL __declspec(dllimport)
#endif

#ifndef BUILD_DLL
#include <ostream>
#endif

namespace EventLib {
    template <typename T> class EventHandler;

    template <typename T>
    class DLL Event {
        friend class EventHandler<T>;

    public:
        /**
         * @brief Construct a new Event object
         * 
         * @param sender Object sending the event
         * @param value Event data
         */
        Event(void* sender, T value);
        ~Event();   //!< Event destructor

        void* sender(); //!< Gets the event sender
        T value();      //!< Gets the event date

        void Cancel();  //!< Cancels the event

        #ifndef BUILD_DLL
        friend std::ostream& operator<<(std::ostream& stream, const Event& e) { //!< Writes the event
            stream << e._value;
            return stream;
        }
        #endif

    private:
        EventHandler<T>* _handler;  //!< EventHandler which created the event
        void* _sender;              //!< Object sending the event
        T _value;                   //!< Event data

        bool* __canceled;           //!< Is the event canceled ?
    };

    template <typename T>
    Event<T>::Event(void* sender, T value): _sender(sender), _value(value), __canceled(nullptr) {}

    template <typename T>
    Event<T>::~Event() {
        __canceled = nullptr;
        _handler = nullptr;
        _sender = nullptr;
    }

    template <typename T>
    void* Event<T>::sender() {
        return _sender;
    }

    template <typename T>
    T Event<T>::value() {
        return _value;
    }

    template <typename T>
    void Event<T>::Cancel() {
        if (__canceled)
            *__canceled = true;
    }
}

#endif // __EVENT_INCLUDED