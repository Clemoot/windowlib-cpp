#ifndef __WINDOW_INCLUDED
#define __WINDOW_INCLUDED

#include <iostream>
#include "basic_window.h"
#include "event_handler.h"

#ifdef BUILD_DLL
#define DLL __declspec(dllexport)
#else
#define DLL __declspec(dllimport)
#endif

namespace WindowLib {
    /**
     * @brief Generic window class
     * The window is mainly composed of EventHandler, which makes it more flexible.
     */
    class DLL Window : public BasicWindow {
    public:
        /**
         * @brief Construct a new Window object
         * @param title Window title
         */
        Window(std::string title);

        EventLib::RestrictedEventHandler<Events::OnCreateEvent> OnCreate; //!< Public EventHandler for OnCreate event
        /**
         * @brief Public EventHandler for OnDestroy event
         * It occurs after OnClose event if it has not been canceled
         */
        EventLib::RestrictedEventHandler<int> OnDestroy;
        EventLib::RestrictedEventHandler<Events::OnWindowChangedEvent> OnMoved;    //!< Public EventHandler for OnMoved event
        EventLib::RestrictedEventHandler<Events::OnWindowChangedEvent> OnResized;  //!< Public EventHandler for OnResized event
        EventLib::RestrictedEventHandler<Events::OnWindowChangedEvent> OnMinimized;    //!< Public EventHandler for OnMinimized event
        EventLib::RestrictedEventHandler<Events::OnWindowChangedEvent> OnMaximized;    //!< Public EventHandler for OnMaximized event
        EventLib::RestrictedEventHandler<Events::OnWindowChangedEvent> OnRestored;     //!< Public EventHandler for OnRestored event
        EventLib::RestrictedEventHandler<Events::OnWindowFocusChangedEvent> OnFocusChanged;    //!< Public EventHandler for OnFocusChanged event
        /**
         * @brief Public EventHandler for OnClose event
         * First event fired when the user wants to close the window.
         * This event is cancelable.
         */
        EventLib::RestrictedEventHandler<int> OnClose;
        /**
         * @brief Public EventHandler for OnQuit event
         * it occurs once the window is destroyed
         */
        EventLib::RestrictedEventHandler<int> OnQuit;
        EventLib::RestrictedEventHandler<int> OnRender;    //!< Public EventHandler for OnRender event
        EventLib::RestrictedEventHandler<Events::OnKeyStateChangedEvent> OnKeyDown;    //!< Public EventHandler for OnKeyDown event
        EventLib::RestrictedEventHandler<Events::OnKeyStateChangedEvent> OnKeyUp;  //!< Public EventHandler for OnKeyUp event
        EventLib::RestrictedEventHandler<Events::OnMouseMoveEvent> OnMouseMove;    //!< Public EventHandler for OnMouseMove event
        EventLib::RestrictedEventHandler<Events::OnMouseButtonChangedEvent> OnMouseButtonDown;     //!< Public EventHandler for OnMouseButtonDown event
        EventLib::RestrictedEventHandler<Events::OnMouseButtonChangedEvent> OnMouseButtonUp;   //!< Public EventHandler for OnMouseButtonUp event
        EventLib::RestrictedEventHandler<Events::OnMouseWheelEvent> OnMouseWheel;  //!< Public EventHandler for OnMouseWheel event

    protected:
        void CB_OnCreate(Events::OnCreateEvent info);
        void CB_OnDestroy();    //!< Function called when the winow is destroyed
        void CB_OnMoved(Events::OnWindowChangedEvent info);
        void CB_OnResized(Events::OnWindowChangedEvent info);
        void CB_OnMinimized(Events::OnWindowChangedEvent info);
        void CB_OnMaximized(Events::OnWindowChangedEvent info);
        void CB_OnRestored(Events::OnWindowChangedEvent info);
        void CB_OnFocusChanged(Events::OnWindowFocusChangedEvent info);
        void CB_OnClose();  //!< Function called when the winow is closed
        void CB_OnQuit(int quitCode);
        void CB_OnRender();     //!< Function called when the winow needs to be rendered
        void CB_OnKeyDown(Events::OnKeyStateChangedEvent info);
        void CB_OnKeyUp(Events::OnKeyStateChangedEvent info);
        void CB_OnMouseMove(Events::OnMouseMoveEvent info);
        void CB_OnMouseButtonDown(Events::OnMouseButtonChangedEvent info);
        void CB_OnMouseButtonUp(Events::OnMouseButtonChangedEvent info);
        void CB_OnMouseWheel(Events::OnMouseWheelEvent info);
        
    private:
        // Event handlers
        EventLib::EventHandler<Events::OnCreateEvent> _OnCreate;  //!< EventHandler when window is created
        EventLib::EventHandler<int> _OnDestroy;   //!< EventHandler when window is destroyed
        EventLib::EventHandler<Events::OnWindowChangedEvent> _OnMoved;    //!< EventHandler when window moves
        EventLib::EventHandler<Events::OnWindowChangedEvent> _OnResized;  //!< EventHandler when window is resized
        EventLib::EventHandler<Events::OnWindowChangedEvent> _OnMinimized;    //!< EventHandler when window is minimized
        EventLib::EventHandler<Events::OnWindowChangedEvent> _OnMaximized;    //!< EventHandler when window is maximized
        EventLib::EventHandler<Events::OnWindowChangedEvent> _OnRestored; //!< EventHandler when window is restored
        EventLib::EventHandler<Events::OnWindowFocusChangedEvent> _OnFocusChanged;    //!< EventHandler when window gains or loses focus
        EventLib::EventHandler<int> _OnClose; //!< EventHandler when window is clsoed
        EventLib::EventHandler<int> _OnQuit;  //!< EventHandler when window is quit
        EventLib::EventHandler<int> _OnRender;    //!< EventHandler when window needs to be rendered
        EventLib::EventHandler<Events::OnKeyStateChangedEvent> _OnKeyDown;    //!< EventHandler when a key is pressed
        EventLib::EventHandler<Events::OnKeyStateChangedEvent> _OnKeyUp;  //!< EventHandler when a key is released
        EventLib::EventHandler<Events::OnMouseMoveEvent> _OnMouseMove;    //!< EventHandler when mouse moves
        EventLib::EventHandler<Events::OnMouseButtonChangedEvent> _OnMouseButtonDown; //!< EventHandler when a mouse button is pressed
        EventLib::EventHandler<Events::OnMouseButtonChangedEvent> _OnMouseButtonUp;   //!< EventHandler when a mouse button is released
        EventLib::EventHandler<Events::OnMouseWheelEvent> _OnMouseWheel;  //!< EventHandler when mouse wheel scrolls
    };
}

#endif // __WINDOW_INCLUDED
