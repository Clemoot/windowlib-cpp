#ifndef __EVENT_INFO_INCLUDED
#define __EVENT_INFO_INCLUDED

#include <memory>
#include <string>

#include "keycodes.h"
#include "rect.h"

namespace WindowLib {
    namespace Events {
        struct OnCreateEvent {  //!< Event data when window is created
            void* hInstance;        //!< Process isntance
            void* parentWindow;     //!< Parent window of the new window
            Rect pos;               //!< Position and size of the window
            std::string windowName; //!< Window title
        };

        struct OnWindowChangedEvent {   //!< Event data when window position or size changed
            int x;  //!< X-coordinate
            int y;  //!< Y-coordinate
            int w;  //!< Width
            int h;  //!< Height
        };

        struct OnWindowFocusChangedEvent {  //!< Event data when window gains or loses focus
            bool hasFocus;  //!< true if the window gains the focus, false if it loses it
            void* otherWindow;  //!< Handle to the other window gaining or losing focus
        };

        struct OnKeyStateChangedEvent { //!< Event data when key state changes (press or release)
            KeyCode keyCode;    //!< Key changing
            char character;     //!< Character associated
            bool isDown;        //!< true if the button get pressed, false if released 
        };

        struct OnMouseMoveEvent {   //!< Event data when mouse moves
            int x; //!< X-coordinate in the window
            int y; //!< Y-coordinate in the window
        };

        struct OnMouseWheelEvent {  //!< Event data when mouse is scrolled
            enum Orientation {  //!< Scroll orientation
                Horizontal, //!< Horizontal scroll
                Vertical    //!< Vertical scroll
            };
            Orientation orientation;  //!< Scroll orientation
            int distance;   //!< Distance scrolled
            int x;          //!< Mouse X-coordinate
            int y;          //!< Mouse Y-coordinate
        };

        struct OnMouseButtonChangedEvent {  //!< Event data when mouse button state changes
            MouseButton button; //!< Mouse button
            bool isDown;        //!< true if the button is pressed, false if released
            int x;              //!< Mouse X-coordinate
            int y;              //!< Mouse Y-coordinate
        };
    }
}

#endif