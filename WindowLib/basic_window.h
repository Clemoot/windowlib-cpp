#ifndef __BASIC_WINDOW_INCLUDED
#define __BASIC_WINDOW_INCLUDED

#ifdef BUILD_DLL
#define DLL __declspec(dllexport)
#else
#define DLL __declspec(dllimport)
#endif

#include <memory>
#include <string>
#include <iostream>

#ifdef WIN32
#include <windows.h>
#endif

#include "rect.h"
#include "eventinfo.h"

namespace WindowLib {
    /**
     * @brief Flags to define window properties
     */
    enum WindowStyleFlags {
        NO_STYLE = 0,
        TITLEBAR = 1,
        RESIZABLE = 2,
        CLOSE_BUTTON = 4,
        MAXIMIZE_BUTTON = 8,
        MINIMIZE_BUTTON = 16,
        ALWAYS_ON_TOP = 32,
        DEFAULT_STYLE = TITLEBAR | RESIZABLE | CLOSE_BUTTON | MAXIMIZE_BUTTON | MINIMIZE_BUTTON,
        ALL_STYLE = TITLEBAR | RESIZABLE | CLOSE_BUTTON | MAXIMIZE_BUTTON | MINIMIZE_BUTTON | ALWAYS_ON_TOP
    };

    /**
     * \brief Abstract BasicWindow class
    */
    class DLL BasicWindow {
    public:
        /**
         * @brief Constructor
         * @param title BasicWindow title
        */
        BasicWindow(std::string title = "Default BasicWindow");
        /**
         * @brief Copy and construct a new BasicWindow object
         * @param BasicWindow Window to copy
         */
        BasicWindow(const BasicWindow& BasicWindow);
        // Destructor
        ~BasicWindow();

        // BasicWindow controls
        /**
         * @brief Creates the window in the operating system/desktop environment
         * @param width Width of the window
         * @param height Height of the window
         * @param style Style of the window
         * @return true if no error occured, false otherwise 
         */
        bool Create(int width, int height, long style = DEFAULT_STYLE);
        /**
         * @brief Shows the window on the screen
         * @return true if the window appeared, false otherwise
         */
        bool Show() const;
        /**
         * @brief Hides the window
         * @return true if the window disappeared, false otherwise
         */
        bool Hide() const;
        /**
         * @brief Destroy the window
         * Usually called when the window is going to close
         * @return true if the window is closed, false otherwise
         */
        bool Destroy();

        /**
         * @brief Minimizes the window
         * @return true if the window got minimized, false otherwise
         */
        bool Minimize();
        /**
         * @brief Maximized the window
         * @return true if the window got maximized, false otherwise
         */
        bool Maximize();
        /**
         * @brief Restores the window
         * @return true if the window got restored, false otherwise
         */
        bool Restore();
        
        /**
         * @brief Handles window messages (blocking)
         * You cannot run this function on another thread
         */
        void WaitMessages();
        /**
         * @brief Handles window messages (non-blocking)
         * You cannot run this function on another thread
         */
        void PollMessages();

        /**
         * @brief Gets window title
         * @return Window Title
         */
        const char* WindowTitle() const;
        /**
         * @brief Sets window title
         * @return true if window title changed, false otherwise
         */
        bool WindowTitle(const char* title);

        /**
         * @brief Gets window handle
         * @return Handle
         */
        void* Handle();

        /**
         * @brief Get window position
         * @return Rectangle with xy-coordinates, width and height
         */
        Rect Pos() const;
        /**
         * @brief Sets window position
         * @return true if the window got moved/resized, false otherwise
         */
        bool Pos(Rect rect);

        /**
         * @brief Whether is the window closed or not
         * @return true if the window is closed, false otherwise
         */
        bool IsClosed() const;

    protected:
        // Event callbacks
        virtual void CB_OnCreate(Events::OnCreateEvent info);    //!< Function called when the winow is created
        virtual void CB_OnDestroy();   //!< Function called when the winow is destroyed
        virtual void CB_OnMoved(Events::OnWindowChangedEvent info);     //!< Function called when the winow is moved
        virtual void CB_OnResized(Events::OnWindowChangedEvent info);   //!< Function called when the winow is resized
        virtual void CB_OnMinimized(Events::OnWindowChangedEvent info); //!< Function called when the winow is minimized
        virtual void CB_OnMaximized(Events::OnWindowChangedEvent info); //!< Function called when the winow is maximized
        virtual void CB_OnRestored(Events::OnWindowChangedEvent info);  //!< Function called when the winow is restored
        virtual void CB_OnFocusChanged(Events::OnWindowFocusChangedEvent info);  //!< Function called when the window gains or loses focus
        virtual void CB_OnClose();     //!< Function called when the winow is closed
        virtual void CB_OnQuit(int quitCode);      //!< Function called when the winow is quit or destroyed
        virtual void CB_OnRender();     //!< Function called when the winow needs to be rendered
        virtual void CB_OnKeyDown(Events::OnKeyStateChangedEvent info);   //!< Function called when a key is pressed
        virtual void CB_OnKeyUp(Events::OnKeyStateChangedEvent info);     //!< Function called when a key is released
        virtual void CB_OnMouseMove(Events::OnMouseMoveEvent info); //!< Function called when the mouse moves
        virtual void CB_OnMouseButtonDown(Events::OnMouseButtonChangedEvent info); //!< Function called when a mouse button is pressed
        virtual void CB_OnMouseButtonUp(Events::OnMouseButtonChangedEvent info); //!< Function called when a mouse button is released
        virtual void CB_OnMouseWheel(Events::OnMouseWheelEvent info);    //!< Function called when the mouse wheel scrolls

        
        Rect _pos;  //!< Position of the window
        int _max_w; //!< Maximum width of the window
        int _max_h; //!< Maximum height of the window 

        const char* _title;    //!< Window title
        bool _isFocused;    //!< Is the  window focused ?
        bool _isClosed; //!< Is the window closed
        void* _handle;  //!< Window handle

    private:
        #ifdef WIN32
            static const char* WINDOW_CLASS_NAME;   //!< Window class for Windows
            static BasicWindow* WindowInCreation;        //!< Window currently in creation

            HINSTANCE _hInstance;   //!< Process instance

            // BasicWindows callbacks for event handling
            static long __stdcall __WindowProc(HWND hWnd, unsigned int msg, unsigned int wParam, long int lParam);
            long __stdcall _WindowProc(HWND hWnd, unsigned int msg, unsigned int wParam, long int lParam);

            // Event handlers (fetching params)
            #define EVENT_HANDLER(function) long int EH_ ## function(HWND hWnd, unsigned int wParam, long int lParam);
            EVENT_HANDLER(OnCreate) //!< Windows-event WM_CREATE handler
            EVENT_HANDLER(OnDestroy) //!< Windows-event WM_DESTROY handler
            EVENT_HANDLER(OnActivate) //!< Windows-event WM_ACTIVATE handler
            EVENT_HANDLER(OnClose) //!< Windows-event WM_CLOSE handler
            EVENT_HANDLER(OnQuit) //!< Windows-event WM_QUIT handler
            EVENT_HANDLER(OnRender) //!< Windows-event WM_PAINT handler
            EVENT_HANDLER(OnKeyDown)    //!< //!< Windows-event WM_KEYDOWN handler
            EVENT_HANDLER(OnKeyUp) //!< Windows-event WM_KEYUP handler
            EVENT_HANDLER(OnMouseMove) //!< Windows-event WM_MOUSEMOVEhandler
            EVENT_HANDLER(OnMouseLeftButtonDown) //!< Windows-event WM_LBUTTONDOWN handler
            EVENT_HANDLER(OnMouseLeftButtonUp)  //!< Windows-event WM_LBUTTONUP handler
            EVENT_HANDLER(OnMouseRightButtonDown)   //!< Windows-event WM_RBUTTONDOWN handler
            EVENT_HANDLER(OnMouseRightButtonUp) //!< Windows-event WM_RBUTTONUP handler
            EVENT_HANDLER(OnMouseWheel) //!< Windows-event WM_MOUSEWHEEL handler
            EVENT_HANDLER(OnMouseWheelHorizontal)   //!< Windows-event WM_MOUSEHWHEEL handler
            EVENT_HANDLER(OnWindowPosChanged) //!< Windows-event WM_WINDOWPOSCHANGED handler
            EVENT_HANDLER(OnMinMaxInfo) //!< Windows-event WM_GETMINMAXINFO handler
            #undef EVENT_HANDLER
        #endif
    };
}

#endif